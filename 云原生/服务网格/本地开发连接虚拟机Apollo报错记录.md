# 问题描述：

<font color=#999AAA >这段时间在学习 Apollo 配置中心，所以在自己的虚拟机上通过 Docker 部署了一套 Apollo 环境。根据 Apollo 官网的步骤一步步进行下来都没有什么问题，Docker 上的 Java  客户端的 Demo 程序也可以运行。但是在自己本地的 IDEA 里跑的 Spring Boot 代码从 Apollo 配置中心获取在线配置的时候启动报错，报错信息如下：

```
2021-01-31 22:19:30.481  INFO 4724 --- [           main] c.l.l.LkjConfigfromapolloApplication     : No active profile set, falling back to default profiles: default
2021-01-31 22:19:31.231  INFO 4724 --- [           main] c.c.f.f.i.p.DefaultApplicationProvider   : App ID is set to lkj-configfromapollo by app.id property from System Property
2021-01-31 22:19:31.231  INFO 4724 --- [           main] c.c.f.f.i.p.DefaultServerProvider        : Environment is set to null. Because it is not available in either (1) JVM system property 'env', (2) OS env variable 'ENV' nor (3) property 'env' from the properties InputStream.
2021-01-31 22:19:31.305  INFO 4724 --- [           main] o.s.cloud.context.scope.GenericScope     : BeanFactory id=1681bfa7-5ebf-3770-b11c-6730b6df4b7e
2021-01-31 22:19:31.363  INFO 4724 --- [           main] c.c.f.a.i.DefaultMetaServerProvider      : Located meta services from apollo.meta configuration: http://192.168.223.134:8080!
2021-01-31 22:19:31.367  INFO 4724 --- [           main] c.c.f.apollo.core.MetaDomainConsts       : Located meta server address http://192.168.223.134:8080 for env UNKNOWN from com.ctrip.framework.apollo.internals.DefaultMetaServerProvider
2021-01-31 22:19:33.468  WARN 4724 --- [           main] c.c.f.a.i.RemoteConfigRepository         : Load config failed, will retry in 1 SECONDS. appId: lkj-configfromapollo, cluster: default, namespaces: application
2021-01-31 22:19:35.474  WARN 4724 --- [           main] c.c.f.a.i.AbstractConfigRepository       : Sync config failed, will retry. Repository class com.ctrip.framework.apollo.internals.RemoteConfigRepository, reason: Load Apollo Config failed - appId: lkj-configfromapollo, cluster: default, namespace: application, url: http://172.18.0.2:8080/configs/lkj-configfromapollo/default/application?ip=192.168.3.12 [Cause: Could not complete get operation [Cause: connect timed out]]
2021-01-31 22:19:36.490  WARN 4724 --- [           main] c.c.f.a.i.RemoteConfigRepository         : Load config failed, will retry in 1 SECONDS. appId: lkj-configfromapollo, cluster: default, namespaces: application
2021-01-31 22:19:38.497  WARN 4724 --- [           main] c.c.f.a.i.LocalFileConfigRepository      : Sync config from upstream repository class com.ctrip.framework.apollo.internals.RemoteConfigRepository failed, reason: Load Apollo Config failed - appId: lkj-configfromapollo, cluster: default, namespace: application, url: http://172.18.0.2:8080/configs/lkj-configfromapollo/default/application?ip=192.168.3.12 [Cause: Could not complete get operation [Cause: connect timed out]]
2021-01-31 22:19:38.509  WARN 4724 --- [ngPollService-1] c.c.f.a.i.RemoteConfigLongPollService    : Long polling failed, will retry in 1 seconds. appId: lkj-configfromapollo, cluster: default, namespaces: application, long polling url: http://172.18.0.2:8080/notifications/v2?cluster=default&appId=lkj-configfromapollo&ip=192.168.3.12&notifications=%5B%7B%22namespaceName%22%3A%22application%22%2C%22notificationId%22%3A-1%7D%5D, reason: Could not complete get operation [Cause: connect timed out]
2021-01-31 22:19:39.499  WARN 4724 --- [           main] c.c.f.a.i.RemoteConfigRepository         : Load config failed, will retry in 1 SECONDS. appId: lkj-configfromapollo, cluster: default, namespaces: application
2021-01-31 22:19:40.514  WARN 4724 --- [ngPollService-1] c.c.f.a.i.RemoteConfigLongPollService    : Long polling failed, will retry in 2 seconds. appId: lkj-configfromapollo, cluster: default, namespaces: application, long polling url: http://172.18.0.2:8080/notifications/v2?cluster=default&appId=lkj-configfromapollo&ip=192.168.3.12&notifications=%5B%7B%22namespaceName%22%3A%22application%22%2C%22notificationId%22%3A-1%7D%5D, reason: Could not complete get operation [Cause: connect timed out]
2021-01-31 22:19:41.505  WARN 4724 --- [           main] c.c.f.a.i.LocalFileConfigRepository      : Sync config from upstream repository class com.ctrip.framework.apollo.internals.RemoteConfigRepository failed, reason: Load Apollo Config failed - appId: lkj-configfromapollo, cluster: default, namespace: application, url: http://172.18.0.2:8080/configs/lkj-configfromapollo/default/application?ip=192.168.3.12 [Cause: Could not complete get operation [Cause: connect timed out]]
```

</font>


<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 原因分析：

一开始是以为自己在 application.yml 中设置的 apollo.mete 的地址有错误，后来通过检查官方文档发现并没有出现问题。之后通过回看官方文档 [Java 客户端使用指南](https://ctripcorp.github.io/apollo/#/zh/usage/java-sdk-user-guide) 发现有个配置没有配置，配置项如下图。

![apollo](E:\中原银行\服务网格\temp\apollo.png)

报错信息中提到的一个 http 请求很关键：

```
http://172.18.0.2:8080/notifications/v2?cluster=default&appId=lkj-configfromapollo&ip=192.168.3.12&notifications=%5B%7B%22namespaceName%22%3A%22application%22%2C%22notificationId%22%3A-1%7D%5D
```

这里面的 172.18.0.2 并不是我物理主机所在的 IP 段，通过进入部署的 Docker 的容器后发现这个 IP 是 Docker 容器的虚拟 IP 地址，所以我将上面 http 请求中的 IP 地址换成我虚拟机的 IP地址之后，成功的返回了相关的配置项：

![apollo1](E:\中原银行\服务网格\temp\apollo1.png)

所以问题的原因找到了，使用 Docker 部署的 Apollo 中，Config Servier 注册到 Meta Server 的是 Docker 的内网地址，本地开发环境无法直接连接，所以解决方法有两种：

- 一、提供从本地开发环境访问 Docker 内网虚拟 IP 的功能，不推荐
- 二、手工声明 Apollo Meta Server 的服务地址，官方文档中也是提供的这种方法

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 解决方案：

在 IDEA 启动项目的参数上加上  -Dapollo.configService=http://192.168.223.134:8080，手工将 Config Server的地址告诉给项目就行了。

![apollo2](E:\中原银行\服务网格\temp\apollo2.png)



# 原理分析

首先要理解上面提到的 Apollo Config Server 是什么东西就得先了解下面的知识才能知晓，首先需要先了解一个 Apollo 配置中心在部署时的相关组件和架构。

## 模块架构

![apollo3](E:\中原银行\服务网格\temp\apollo3.png)

上图简要描述了 Apollo 的总体设计，我们可以从下往上看：

1. Config Service 提供配置的读取、推送等功能，服务对象是 Apollo 客户端（我们自己的微服务应用）
2. Admin Service 提供配置的修改、发布等功能，服务对象是 Apollo Portal（管理界面）
3. Config Service 和 Admin Service 都是多实例、无状态部署，所以需要将自己注册到Eureka中并保持心跳
4. 在 Eureka 之上我们架了一层 Meta Server 用于封装Eureka的服务发现接口
5. Client 通过域名访问 Meta Server 获取 Config Service 服务列表（IP+Port），而后直接通过 IP+Port 访问服务，同时在Client侧会做load balance、错误重试
6. Portal 通过域名访问Meta Server获取A dmin Service 服务列表（IP+Port），而后直接通过 IP+Port 访问服务，同时在 Portal 侧会做 load balance、错误重试
7. 为了简化部署，我们实际上会把 Config Service、Eureka 和 Meta Server 三个逻辑角色部署在同一个 JVM 进程中。



## 核心概念：

1. **application (应用)**
   这个很好理解，就是实际使用配置的应用，Apollo客户端在运行时需要知道当前应用是谁，从而可以去获取对应的配置每个应用都需要有唯一的身份标识 – appId，我们认为应用身份是跟着代码走的，所以需要在代码中配置。
2. **environment (环境)**
   配置对应的环境，Apollo客户端在运行时需要知道当前应用处于哪个环境，从而可以去获取应用的配置我们认为环境和代码无关，同一份代码部署在不同的环境就应该能够获取到不同环境的配置所以环境默认是通过读取机器上的配置（server.properties中的env属性）指定的，不过为了开发方便，我们也支持运行时通过System Property等指定。
3. **cluster (集群)**
   一个应用下不同实例的分组，比如典型的可以按照数据中心分，把上海机房的应用实例分为一个集群，把北京机房的应用实例分为另一个集群。对不同的cluster，同一个配置可以有不一样的值，如zookeeper地址。
   集群默认是通过读取机器上的配置（server.properties中的idc属性）指定的，不过也支持运行时通过System Property指定，具体信息请参见Java客户端使用指南。
4. **namespace (命名空间)**
   一个应用下不同配置的分组，可以简单地把namespace类比为文件，不同类型的配置存放在不同的文件中，如数据库配置文件，RPC配置文件，应用自身的配置文件等。



# 总结

这次遇到的问题虽然不是很严重，但是想要彻底弄懂这个问题，是需要我们理解 Apollo 配置中心在部署时的整体架构，这样我们才能知晓官方文档中安排的 [跳过Apollo Meta Server服务发现](https://ctripcorp.github.io/apollo/#/zh/usage/java-sdk-user-guide?id=_1222-跳过apollo-meta-server服务发现) 这一小节的目的所在。一个 Apollo 配置中心不只是一个应用程序而已，它也有自己的注册中心、负载均衡负载均衡策略，这些内部的细节还是需要多多学习才能彻底弄懂 Apollo 的运行机制。

相关链接：

- [Apollo 官方文档](https://ctripcorp.github.io/apollo/#/zh/README)
- [Apollo配置中心(一)](https://blog.csdn.net/hong10086/article/details/106982995)