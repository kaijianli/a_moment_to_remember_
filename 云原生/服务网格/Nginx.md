# 一、路由--Location的使用

## 1、Location语法规则

语法规则： location [=|~|~*|^~] /uri/ {… }

首先匹配 =，其次匹配^~,其次是按文件中顺序的正则匹配，最后是交给 /通用匹配。当有匹配成功时候，停止匹配，按当前匹配规则处理请求。

| 符号    | 含义                                                         |
| ------- | ------------------------------------------------------------ |
| =       | = 开头表示精确匹配                                           |
| ^~      | ^~开头表示uri以某个常规字符串开头，理解为匹配 url路径即可（禁止正则匹配）。 |
| ~       | ~ 开头表示区分大小写的正则匹配                               |
| ~*      | ~* 开头表示不区分大小写的正则匹配                            |
| !~和!~* | !~和!~*分别为区分大小写不匹配及不区分大小写不匹配的正则      |
| /       | 用户所使用的代理（一般为浏览器）                             |

 

匹配规则优先级如下：

![nginx](E:\中原银行\服务网格\temp\nginx.png)



-  =精准匹配命中时，停止location动作，直接走精准匹配，
-  一般匹配（含非正则）命中时，先收集所有的普通匹配，最后对比出最长的那一条
-  如果最长的那一条普通匹配声明为非正则，直接此条匹配，停止location
-  如果最长的那一条普通匹配不是非正则，继续往下走正则location
- 按代码顺序执行正则匹配，当第一条正则location命中时，停止location

 

## 2、path匹配过程



![nginx的path](E:\中原银行\服务网格\temp\nginx的path.png)



假设http请求路径为   http://192.168.0.132:8088/mvc/index?id=2 ，匹配过程如下：

- 将整个url拆解为域名/端口/path/params

- 先由域名/端口，对应到目标server虚拟主机
- path部分参与location匹配，path = path1匹配部分 + path2剩余部分

- 进入location方法体内部流程。

- 若是静态文件处理，则进入目标目录查找文件：root指令时找path1+path2对应的文件;alias指令时找path2对应的文件

- 若是proxy代理，则形如proxy_pass=ip:port时转发path1+path2路径到tomcat;形如proxy_pass=ip:port/xxx时转发path2路径到tomcat。params始终跟随转发。

![nginx1](E:\中原银行\服务网格\temp\nginx1.png)

![nginx1](E:\中原银行\服务网格\temp\nginx2.png)

# 二、rewrite使用：

**rewrite 语法规则：rewrite regex replacement [flag];**  

**flag=[break/last/redirect/permanent]** 

- regex 是正则表达式

- replacement 是替换值，新值

- flag -- 后续处理标识

## 1、flag=break

发生nginx内部重定向，path值被更新，rewrite层面的命令会中断。原控制流程逻辑不变往下走

## 2、flag=last

发生nginx内部重定向，path值被更新，rewrite层面的命令会中断。控制流程刷新，重新进行整个location层的逻辑流程。

## 3、flag= redirect/permanent

发生页面重定向（301永久重定向/302临时重定向），nginx流程结束，返回http响应到浏览器，页面url更新

## 4、flag为空

发生nginx内部重定向，path值被更新，rewrite层面的命令继续。最后一个rewrite完毕，刷新控制流程，重新进行location重匹配

# 三、Nginx处理请求的11个阶段



![nginx1](E:\中原银行\服务网格\temp\nginx3.png)



Nginx 处理请求的全过程一共划分为 11 个阶段（如图），**按阶段由上到下依次执行 （上一阶段的所有指令执行完毕，才进入下一阶段）**

**各阶段的含义如下：**

- post-read: 接收到完整的http头部后处理的阶段，在uri重写之前。一般跳过 

- server-rewrite: location匹配前，修改uri的阶段，用于重定向，location块外的重写指令（**多次执行**） 

- find-config: uri寻找匹配的location块配置项（**多次执行**） 

- rewrite: 找到location块后再修改uri，location级别的uri重写阶段（**多次执行**） 

- post-rewrite: 防死循环，跳转到对应阶段 

- preaccess: 权限预处理

- access: 判断是否允许这个请求进入 

- post-access: 向用户发送拒绝服务的错误码，用来响应上一阶段的拒绝 

- try-files: 访问静态文件资源 

- content : 内容生成阶段，该阶段产生响应，并发送到客户端 

- log: 记录访问日志 

 

 