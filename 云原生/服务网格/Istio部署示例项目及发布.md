# Istio部署示例项目及发布

 Istio部署完环境之后就可以进行示例项目bookinfo的部署以及发布。该应用程序显示有关书籍的信息，类似于在线书籍商店的单个目录条目。页面上显示的是书籍说明，书籍详细信息（ISBN，页数等）以及一些书籍评论。 

Bookinfo应用程序分为四个单独的微服务：

- `productpage`。该`productpage`微服务调用`details`和`reviews`微服务来填充页面。
- `details`。该`details`微服务包含图书信息。
- `reviews`。该`reviews`微服务包含了书评。它还称为`ratings`微服务。
- `ratings`。该`ratings`微服务包含预定伴随书评排名信息。

`reviews`微服务有3个版本：

- 版本v1不会调用该`ratings`服务。
- 版本v2调用该`ratings`服务，并将每个等级显示为1到5个黑星。
- 版本v3调用该`ratings`服务，并将每个等级显示为1到5个红色星号。

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

## Bookinfo体系结构

Bookinfo应用程序的端到端体系结构如下所示。 

![bookinfo](C:\Users\李凯建\Desktop\temp\temp\bookinfo.svg)

每个微服务都将与一个Envoy Sidecar打包在一起，该Envoy Sidecar拦截对服务的入流量和出流量，通过Istio控制平面、路由、遥测收集和整个应用程序的策略实施来提供外部控制所需的入口。 


<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">
## 部署应用程序

 如果尚未安装Istio，请按照[官网](https://istio.io/latest/docs/setup/)的说明进行[Istio框架的安装](https://istio.io/latest/docs/setup/)。 

### 安装步骤

1.  进入Istio的安装目录中，以我的Istio安装路径为例

   ```bash
   $ cd ~/istio-1.8.1/
   ```

2. Istio默认会自动注入Sidecar。但是在default的namespase中并不会默认注入Sidecar，需要使用以下命令标记namespace来实现Sidecar的自动注入： 

   ```bash
   $ kubectl label namespace default istio-injection=enabled
   ```

3. 然后进行bookinfo的项目部署。部署时需要Istio官方提供的默认的bookinfo的yaml文件，该文件已经在Istio安装目录的`samples/`文件夹下进行了提供。通过使用`kubectl apply -f <filename>` 命令来进行一个项目的部署。

   ```bash
   $ kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml
   ```

    该命令将启动`bookinfo`应用程序体系结构图中显示的所有四个服务。已启动评论服务的所有3个版本，即v1，v2和v3。 

   *注：在安装过程中由于网络原因以及机器性能的原因，docker 在实例化容器的时候会有一定的等待时间，在这段时间内还需要耐心等待，可以使用 `kubectl get pods -A` 命令查看pod的启动情况，当所有pod的状态都为 `Running` 的时候项目就启动完成了，如下图所示。*

   ![bookinfo1](C:\Users\李凯建\Desktop\temp\temp\bookinfo1.png)

4.  确认所有服务已正确定义并正在运行： 

   ```bash
   $ kubectl get services
   ```

   结果如图所示。

   ![bookinfo2](C:\Users\李凯建\Desktop\temp\temp\bookinfo2.png)

5.  要确认Bookinfo应用程序正在运行，请通过`curl`某个Pod中的命令向其发送请求，例如`ratings`： 

   ```bash
   $ kubectl exec "$(kubectl get pod -l app=ratings -o jsonpath = '{.items[0].metadata.name}')" -c ratings -- curl productpage:9080/productpage | grep -o "<title>.*</title>"
   ```

   结果如下所示的话就恭喜你，你的bookinfo已经在Istio框架中部署好了。

   ```html
   <title>Simple Bookstore App</title>
   ```

   

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

### 确定入口IP和端口

现在Bookinfo服务已启动并正在运行，您需要从Kubernetes集群外部访问该应用程序，例如，从浏览器访问。此时就需要定义一个网关。 

1. 从Istio提供的示例文件中定义应用程序的入口网关：

   ```bash
   $ kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
   ```

   

2. 确认网关已创建：

   ```bash
   $ kubectl get gateway
   ```

   结果如下。

   ```bash
   NAME               AGE
   bookinfo-gateway   32s
   ```

   

3. 现在网关已经创建成功，但是我们还需要获得从外部进行访问网关的地址。我们可以通过以下步骤获取网关的外部访问地址。这里以我自己电脑上的虚拟机为例。（本 k8s 集群使用的是`kubectl` 的方式进行的部署）

   本地虚拟机是没有使用外部负载均衡器的。如果你使用的是云服务器并且提供了外部负载均衡器，可以参照[官网](https://istio.io/latest/docs/setup/getting-started/#determining-the-ingress-ip-and-ports)的教程进行设置。

   设置`INGRESS_HOST`和`INGRESS_PORT`变量以访问网关。

   ```bash
   $ export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
   $ export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
   $ export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')
   ```

   

4. 设置`GATEWAY_URL`：

   ```bash
   $ export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT
   ```

   

5. 运行以下命令以获取Bookinfo应用程序的外部地址。将该输出粘贴到Web浏览器中，并确认已显示Bookinfo产品页面。

   ```bash
   $ echo "外部访问地址：http://$GATEWAY_URL/productpage"
   ```

   访问结果如图所示。

   ![bookinfo3](C:\Users\李凯建\Desktop\temp\temp\bookinfo3.png)



<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

### 删除bookinfo项目

当你完成Bookinfo示例的实验后，可以按照以下说明进行卸载和清理：

1. 通过 Istio 提供的脚本删除 bookinfo 的路由规则并终止应用程序容器

   ```bash
   $ samples/bookinfo/platform/kube/cleanup.sh
   ```

   

2. 对于有强迫症的人来说可以执行以下命令确认已经清理干净

   ```bash
   $ kubectl get virtualservices   #-- there should be no virtual services
   $ kubectl get destinationrules  #-- there should be no destination rules
   $ kubectl get gateway           #-- there should be no gateway
   $ kubectl get pods              #-- the Bookinfo pods should be deleted
   ```

   

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

### 遇到的问题

- 可以查看 bookinfo.yaml 文件的内容查看

  ```bash
  $ cat samples/bookinfo/platform/kube/bookinfo.yaml
  ```

  其中只是进行了一些 Service 和 Deployment的定义。

- 部署 Bookinfo 示例之后并没有自动注入Envoy Sedecar

  查看default的label中是否有 `istio-injection: enabled`  标志

  ```bash
  $ kubectl get ns default -o yaml
  ```

  如果没有的话还需要重复执行安装步骤中的第2步。

- 部署之后等了很长时间 `Pod` 的状态都没有变成 `Running` 状态

  考虑是否是 `node` 节点的通讯问题，可以重启以下 `node` 节点



<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

### 参考链接

[Istio官网地址](https://istio.io/latest/docs/examples/bookinfo/#before-you-begin)

