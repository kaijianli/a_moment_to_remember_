# 使用Istio进行项目的灰度发布
# 一、什么是灰度发布？

**灰度发布又叫金丝雀发布：**

- **来源：**灰度发布来源于这样一个典故：17世纪的时候，英国矿井工人他们偶然的发现金丝雀这种鸟类对瓦斯气体非常的敏感，大家都知道，在矿井这种地下作业的时候，瓦斯这种气体经常会出现，大量的吸入会对人体有害，所以这些矿井工人每次在进行作业的时候都会带上一只金丝雀鸟，用它来做一个检测，一旦检测到瓦斯存在，金丝雀轻则不会再鸣叫，重则就容易身亡。所以引申出来的概念就是把金丝雀部署这样的一个小范围测试、小范围发布的方式来逐渐更新服务的版本。
- **最终目的：**最终要求把所有的流量打向新的版本，实现一个版本的更迭。
- **优点：**可以利用真实的线上数据进行一个测试，并且也是比较容易的进行回滚的。
- **演示：**利用上一节安装的`bookinfo`官方示例项目，进行灰度发布的模拟部署，实现流量转移。

实现灰度发布的核心步骤就在于访问流量的转移，举个例子：一开始所有的流量访问的都是系统中的A服务，但是由于系统版本更迭，现在A服务的另一个版本要进行上线（称为A2服务），为了更加平滑的进行系统的升级，在初期可以通过配置 Istio中的 `VirtualService` ，将A服务的5%的流量分到A2服务中，如果没有问题可以持续增大分到A2服务中流量的比例，直到所有的流量最终都访问到A2服务中。



<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">



# 二、操作步骤

- 利用reviews服务的多版本，模拟灰度发布

- 在VirtualService中配置权重

- 浏览器中测试

  <hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

## 执行操作脚本

在上一篇文章中，部署了 `bookinfo` 应用，`bookinfo` 中的 `reviews` 服务一共发布了三个版本，我们可以利用其中的 `v1` 和 `v3` 两个版本进行访问流量的转移（灰度发布）。如下图所示。

![bookinfo4](E:\中原银行\服务网格\temp\bookinfo4.png)

Istio 中的流量转移是通过 `VirtualService` 和 `DestinationRole` 来实现的，所以我们需要先将 `bookinfo` 中每个服务的 `VirtualService` 和 `DestinationRole` 部署到我们的服务器中。

**先将所有的流量都指向 reviews 的 v1 版本**

Istio 官方已经提供了相关的部署脚本，我们直接使用即可。在 master 节点中，进入到Istio的安装目录中，执行以下命令部署指向 `v1` 版本的 `VirtualService`

```bash
# kubectl apply -f samples/bookinfo/networking/virtual-service-all-v1.yaml
```

执行以下命令部署 bookinfo 的 DestionationRule（目标服务）

```bash
# kubectl apply -f samples/bookinfo/networking/destination-rule-all.yaml
```

执行完以上两条命令之后所有访问 `bookinfo` 系统的流量都会只访问 reviews 的 v1 版本，可以通过多次访问外部浏览器，出现的都是如下界面即证明设置成功。

**获得从浏览器访问 bookinfo 系统的url方法可以参考上一篇的博客：**[Istio部署示例项目及发布](https://blog.csdn.net/weixin_45207474/article/details/112452400)

![bookinfo3](E:\中原银行\服务网格\temp\reviewsv1.png)

**通过设置 `DestinationRule` 的权重进行流量的转移**

在 `master` 节点中执行如下命令：

```bash
# kubectl apply -f - <<EOF
> apiVersion: networking.istio.io/v1alpha3
> kind: VirtualService
> metadata:
>   name: reviews
> spec:
>   hosts:
>     - reviews
>   http:
>   - route:
>     - destination:
>         host: reviews
>         subset: v1
>       weight: 50
>     - destination:
>         host: reviews
>         subset: v3
>       weight: 50
> EOF
```

此版本的 `VirtualService` 信息 Istio 官方也已经以 yaml 脚本的形式提供给了我们。进入 Istio 的安装目录中，通过以下命令可以展示：

```bash
# cat samples/bookinfo/networking/virtual-service-reviews-50-v3.yaml
```



<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">



## 浏览器验证

通过浏览器访问多次 bookinfo 系统并进行记录，发现 reviews 的 `V1` 和 `v3` 版本没有规律的交替出现，但是两个版本出现的概率分别为 50% 和 50% ，出现的概率即为 `virtual-service-reviews-50-v3.yaml` 这个脚本中对目标服务权重的设置比重。



<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">
# 分析

我们可以看一下刚才执行的关键命令：

```bash
# kubectl apply -f - <<EOF
> apiVersion: networking.istio.io/v1alpha3
> kind: VirtualService
> metadata:
>   name: reviews
> spec:
>   hosts:
>     - reviews
>   http:
>   - route:
>     - destination:
>         host: reviews
>         subset: v1
>       weight: 50  # 权重的设置
>     - destination:
>         host: reviews
>         subset: v3
>       weight: 50  # 权重的设置
> EOF
```

这个配置其实非常简单，它只不过对 reviews 服务的 `VirtualService` 指向的两个不同的 `DestinationRule` （目标服务）添加了 `weight` 这样的一个权重的设置，使得访问流量的 50% 流向了 `v1` 版本，另外的 50% 流向了 `v3` 版本。

所以实现灰度发布你只需要不断的调整新旧两个版本的对应的权重，最终就能实现一个灰度发布的过程。

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 总结

以上就是今天要讲的内容，本文仅仅介绍了灰度发布的简单实现使用，目前市面上也有很多的自动化的灰度发布工具，可以通过配置动态的进行权重调整的过程，不需要我们手动的去进行修改。比如来源与 weaveworks的 `Flagger` 自动化灰度发布工具，有兴趣的同学可以去了解一下。