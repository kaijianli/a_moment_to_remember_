# Kubernetes学习笔记
<font color=#999AAA >对最近学习的一些 K8S 的一些知识点做的整理和总结。
</font>



<font color=#999AAA >提示：写完文章后，目录可以自动生成，如何生成可参考右边的帮助文档</font>

@[TOC](文章目录)


# Kubernetes基本概念和术语

<font color=#999AAA >Kubernetes 中的大部分概念如 Node Pod Replication Controller Service 等都可以看作一
种“资源对象”，几乎所有的资源对象都可以通过 Kubernetes 提供的 kubectl 工具（或者 API
程调用）执行增、删、改、查等操 井将其保存在 etcd 中持久化存储。从这个角度来看， Kubernetes
其实是一个高度自动化的资源控制系统，它通过跟踪对比 etcd 库里保存的“资源期望状态”与
当前环境中 的“实际资源状态”的差异来实现自动控制和自动纠错的高级功能。
</font>



## Master

 Kubernetes 里的 Master 指的是集群控制节点 每个 Kubernetes 集群里需要有一个 Master 节点来负责整个集群的管理和控制，基本上 Kubernetes 的所有控制命令都发给它，它来负责具体的执行过程，我们后面执行的所有命令基本都是在 Master 节点上运行的。Master 节点是整个集 群的 首脑”，如果岩机或者不可用 ，那么对集群内容器应用的管理都将失效。 

 **Master 点上运行着一些关键进程**： 

- Kubernetes API Server (kube iserver ):  提供了 HTTP Rest 接口的关键服务进程，是 Kubernetes 里所有资源的增、删、改、 查等操作的唯一入口，也是集群控制的入口进程。
- Kubernetes Controller Manager ( kube-controller-manager):  Kubernetes 里所有资源对象的自动化控制中心，可以理解为资源对象的“大总管” 。
- Kubernetes Scheduler C !rube-scheduler ）： 负责资源调度（ Pod 调度〉的进程，相当于公交公司的 "调度室" 

另外，在 Master 节点上还需要启动一个 etcd 服务，因为 Kubernete 里的所有资源对象的数据全部是保存在 etcd 中的。

## Node

 Kubernetes 集群中的其他机器被称为 Node 节点，Node 节点才是Kubernetes 集群中的工作负载节点，每个 Node 会被 Master 分配一些工作负载（ Docker 容器），当某个 Node 岩机时，其上的工作负载会被 Master 自动转移到其他节点上去。 

 **Node 节点上都运行的关键进程：**

-  kubelet ：负则Pod 容器的创建、启停等任务，同时与 Master 节点密切协作， 现集群管理的基本功能。 
- kube-proxy：实现 Kubernetes Service 的通信与负载均衡机制的重要组件。
- Docker Engine ( docker) : Docker 引擎，负责本机的容器创建和管理工作。 

在默认情况下Node中的 kubelet进程会向 Master 注册自己（这也是 Kubernetes 推荐的 Node 管理方式）。一旦 Node 被纳入集群管理范围， kubelet 进程就会定时向 Master 汇报自身的情报，例如操作系统、 Docker 版本、机器的 CPU 和内存情况，以及当前有哪些 Pod 在运行。 这样 Master 可以获知每 Node 的资源使用情况，并实现高效均衡的资源调度策略。 而某个 Node 超过指定时间不上报信息时，会被 Master 判定为“失联”， Node 的状态被标记为不可用（Not Ready ），随后 Master 会触发“工作负载大转移”的自动流程。

**在master节点上常用的查看node的命令：**

```bash
$ kubectl get nodes  #查看集群中节点的简略信息
$ kubectl describe node <node_name>  #可以查看集群中某个节点的详细信息
```

## Pod

 Pod是Kubernetes 的最重要也最基本的概念，每个 Pod 都有一个特殊的被称为“根容器”的 Pause 容器，Pause 容器对应的镜像属于Kubenetes 平台的一部分，除了 Pause 容器，每个 Pod 还包含一个或多个紧密相关的用户业务容器。  

![1](C:\Users\李凯建\Desktop\temp\temp\1.png)

**Pod出现的必要性**

- 原因之一 ：在一组容器作为一个单元的情况下，我们难以对“整体”简单地进行判断及有 效地进行行动。比如， 一个容器死亡了，此时算是整体死亡么？是 N/M 的死亡率么？引入业务无关井且不易死亡的 Pause 容器作为 Pod 的根容器，以它的状态代表整个容器组的状态，就简单、巧妙地解决了这个难题。

- 原因之二：Pod 里的多个业务容器共享 Pause 容器的IP，共享 Pause 容器挂接的 Volume, 这样既简化了密切关联的业务容器之间的通信问题，也很好地解决了它们之间的文件共享问题。

Kubenetes 为每个 Pod 都分配了唯一的IP地址，称之为 Pod IP。Pod 里的多个容器共 Pod IP 地址。Kubernetes 要求底层网络支持集群内任意两个 Pod 之间的 TCP/IP 直接通信，这通常采用虚拟二层网络技术来实现，因此我们需要牢记一点： 在Kubernetes 里，Pod 里的容器与另外主机上的 Pod 容器能够直接通信。

Pod 其实有两种类型：普通的 Pod 及静态 Pod (Static Pod ），后者比较特殊，它并不存放在 Kubernetes etcd 存储里，而是存放在某个具体 Node 上的一个具体文件中，并且只在此 Node 上启动运行，不会参与调度。 而普通的 Pod 一旦被创建，就会被放入到 etcd 中存储，随后会被 Kubernetes Master 调度到某个具体的 Node 上并进行绑定（ Binding ），随后该 Pod 被对应的 Node 上的 kubelet 进程实例化成一组相关 Docker 容器并启动起来。在默认情况下，当 Pod 里的某个容器停止时 Kubernetes 会自动检测到这个问题并且重新启动这 Pod （重启 Pod 里的所有容器），如果Pod 所在的 Node 岩机，则会将这个 Node 上的所有 Pod 重新调度到其他节点上。 Pod 、容器与 Node 的关系如图所示。 

![2](C:\Users\李凯建\Desktop\temp\temp\2.png)

**Pod 的简单描述文件**

Kubernetes 的所有资源对象都可以采用 yaml 或者 JSON 格式的文件来定义或描述。 

```yaml
apiVersion : v1
kind: Pod
metadata:
  name: myweb
  labels:
    name: myweb
spec :
  containers:
  - name: myweb
    image: tomcat
    ports:
    - containerPort: 8080
    env:
    - name: MYSQL SERVICE HOST
      value: 'mysql'
    - name: MYSQL SERVICE PORT
      value: '3306'
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
```

- Kind 为 Pod 表明这是一个 Pod 的定义
- metadata 里的 name 属性为 Pod 的名字， metadata 里还能定义资源对象的标签（ Label ），这里声明 myweb 拥有一个 name=myweb 的标签（ Label ）。 
- Pod 里所包含的容器组的定义则在 spec 一节中声明，这里定义了一个名字为 myweb、对应镜像 为tomcat 的容器，该容器注入了名为 MYSQL_SERVICE_ HOST='mysql’ 和 MYSQL _SERVICE _PORT＝’3306’ 的环境变量（env 关键字），并且在 8080 端口（container Port) 上启动容器进程。 
- Pod 的 IP 加上这里的容器端口（ containerPort ），就组成了 个新的概念一 Endpoint ，它代表着此 Pod 里的一个服务进程的对外通信地址。
- 一个 Pod 也存在着具有多个 Endpoint 的情况，比如当我们把 Tomcat 定义为一个 Pod 时，可以对外暴露管理端口与服务端口这两个 Endpoint。
- 我们所熟悉的 Docker Volume 在 Kubernetes 里也有对应的概念——Pod Volume ，后者有些扩展，比如可以用分布式文件系统实现后端存储功能： Pod Volume 是定义在 Pod 之上，然后被各个容器挂载到自己的文件系统中的。 
- 在Kubernetes 里，一个计算资源进行配额限定需要设定以下两个参数：
  - Requests ：该资源的最小申请量，系统必须满足要求。
  - Limits ：该资源最大允许使用的量，不能被突破，当容器试图使用超过这个量的资源时， 可能会被 Kubernetes Kill 并重启。 
  - 在Kubernetes 里，通常以千分之 CPU 配额为最小单位，用m来表示。通常一个容器的 CPU 配额被定义 100-300m ，即占用 0.1 0.3 CPU 。由于 CPU 配额是一个绝对值，所以无论在拥有一个 Core 的机器上，还是在拥有 48 Core 的机器上， 100m 这个配额所代表的 CPU 的使用量都是 一样的。与 CPU 配额类似， Memory 配额也是一个绝对值，它的单位是内存字节数 

## Label（标签）

 Kubernetes 系统中的 Label 是一个 key=value 的键值对，其中 key 与 value 由用户自己指定。 Label 可以附加到各种资源对象上，例如 Node、Pod、Service、RC 等，一个资源对象可以定义任意数量的 Label ，同一个 Label 也可以被添加到任 意数量的资源对象上去，Label 通常在资源对象定义时确定，也可以在对象创建后动态添加或者删除。

我们可以通过给指定的资源对象捆绑一个或多个不同的 Label 来实现多维度的资源分组管理功能，以便于灵活、方便地进行资源分配、调度、配置、部署等管理工作。例如：部署不同版本的应用到不同的环境中；或者监控和分析应用（日志记录、监控、告警）等。

Label 相当于我们熟悉的“标签”，给某个资源对象定义 Label ，就相当于给它打了 一个标签，随后可以通过 Label Selector （标签选择器〉查询和筛选拥有某些 Label 的资源对象。

当前有两种 Label Selector 的表达式：

- 基于等式的（ Equality-based ）：
  - name = redis-slave ：匹配所有具有标签 name edis-slave 的资源对象 
  - env != production ：匹配所有不具有标签 env=production 的资源对象，比如 env=test 是满足此条件的标签之一。 

- 基于集合的（Set-based ）：
  - name in (red is-master redis-slave ）：匹配所有具有标签 name=redis-master 或者 name= redis-slave 的资源对象。
  - name not in ( php－frontend ）：匹配所有不具有标签 name=php－frontend 的资源对象。

可以通过多个 Label Selector 表达式的组合实现复杂的条件选择，多个表达式之间用“，” 进行分隔即可，几个条件之间是“AND ”的关系，即同时满足多个条件。 

**使用Label 的简单示例**

以mywe Pod 为例， Label 定义在其 metadata 中：

```yaml
apiVersion : v1
kind: Pod
metadata:
  name : myweb
  labels:
    app : myweb 
```

 管理对象 RC 和 Service 在 spec 中定义 `selector `与 Pod 进行关联：

```yaml
# RC
apiVersion : v1
kind : ReplicationController
metadata:
  name: myweb
spec :
  replicas : 1
  selector:
    app : myweb
  template: 
  # 以下省略
  
# Service
apiVersion : v1
kind : Service
metadata :
  name: myweb
spec:
  selector:
    app : myweb
  ports:
  - port : 8080
```

新出现的管理对象如 Deployment、ReplicaSet、Daemon Set 和 Job 则可以在 Selector 中使用基于集合的筛选条件定义，例如：

```yaml
selector:
  matchLabels :
    app: myweb
  matchExpressions:
    - {key: tier , perator In , values: [frontend]}
    - {key: environment , operator : Notin , values: [dev]} 
```

matchLabels 于定义一组 Label，与直接写在 Selector 作用相同。matchExpressions 于定义一组基于集合的筛选条件，可用的条件运算符包括 ：In、NotIn、Exists 和 DoesNotExist。 如果同时设置了 matchLabel 和 matchExpressions ，则两组条件为 “AND ” 关系，即所有条件需要同时满足才能完成 Selector 的筛选 。

**Label 的使用场景**

- kube-controller 进程通过资源对象 RC 上定义的 Label Selector 来筛选要监控的 Pod 副本的数量 ，从而实现 Pod 副本的数量始终符合预期设定的全自动控制流程。
- kube-proxy 进程通过 Service 的 Label Selector 来选择对应的 Pod ，自动建立起每个 Service 到对应 Pod 的请求转发路由表，从而实现 Service 的智能负载均衡机制。
- 通过对某些 Node 定义特定的 Label ，并且在 Pod 定义文件中使用 NodeSelector 这种标签调度策略， kube heduler 进程可以实现 Pod “定向调度” 的特性。

## Replication Controller

RC (Replication Controller) 是Kubenetes 系统中的核心概念之一，简单来说，它其实是定义了一个期望的场景， 即声明某种 Pod 的副本数量在任意时刻都符合某个预期值，所以 RC 的定义包括如下几个部分：

- Pod 期待的副本数 ( replicas )。 
- 用于筛选目标 Pod 的 Label Selector。
- 当 Pod 的副本数量小于预期数量时，用于创建新 Pod 的 Pod 模板 ( template ) 。

**RC 的简单描述文件**

```yaml
apiVersion: v1
kind: ReplicationController  # 副本控制器 RC
metadata:
  name: mysql                # RC 的名称，全局唯一
spec:
  replicas: 1                # Pod 副本期待数量
  selector:
    app: mysql               # Pod 副本拥有的标签，对应 RC Selector
template:                    # Pod 模板信息定义
  metadata:
    labels:
      app: mysql
  spec:
    containers:
    - name: mysql           # 容器的名称
      image: mysql          # 容器对应的 Docker Image 
      ports:
      - containerPort: 3306
      env :
      - name: MYSQL ROOT PASSWORD      # 注入容器内的环绕变量
        value: "123456"
```

- kind 属性，用来表明此资源对象的类型，比如这 的值为 ReplicationController ”，表示这是一个 RC 。
- spec 一节中是 RC 的相关属性定义，比如 spec.selector 是 RC 的 Pod 标签（ Label) 选择器，即监控和管理拥有这些标签的 Pod 实例。
- spec.replicas 确保当前集群上始终有且仅有 replicas 个 Pod 实例在运行，这里我们设置 replicas= 1 表示只能运行一个 MySQL Pod 实例。
- 当集群中运行 Pod 小于 replicas 时， RC 会根据 spec. template 一节中定义的 Pod 模板来生成一个新的 Pod 实例。
- spec. template.metadata.labels 指定了该 Pod 的标签，需要特别注意 ：这个 labels 必须匹配之前的 spec.selector ，否则此 RC 每次创建了一个无法匹配 Label 的 Pod, 就会不停地尝试创建新的 Pod ，最终陷入“为他人作嫁衣裳”的悲惨世界中，永无翻身之时。 

当我们定义了一个 RC 井提交到 Kubenetes 集群中以后， Master 节点上的 Controller Manager 组件就得到 通知，定期巡检系统中当前存活的目标Pod，并确保目标Pod实例的数量刚好等于此RC的期望值，如果有过多的Pod副本在运行，系统就会停掉一些Pod，否则系统就会再创建一些Pod。

 由于 Replication Controller 与 Kubenetes 代码中的模块 Replication Controller 同名，同时这个词也无法准确表达它的本意，所以在 Kubenetes v 1.2 时，它就升级成了另外一个新的概 念—Replica Set ，它与 RC 当前存在的唯一区别是： Replica Sets 支持基于集合的 Label selector ( Set-based selector ），而 RC 只支持基于等式的 Label Selector (equality-based selector ），这使得 Replica Set 的功能更强。  

**RC 操作的基本命令**

```bash
$ kubectl create -f <rc_name>.yaml           # 根据要创建的 rc 的 yaml 文件创建一个 RC 实例
$ kubectl get rc                             # 查看创建的RC实例信息
$ kubectl scale re redis-slave --replicas=3  # 动态修改RC规定的Pod副本数量，实现动态缩放
$ kubectl delete replicationcontroller <rc_name>   # 删除指定的rc实例对象
```

 **RC 的一些特性与作用**

- 大多数情况下 ，我们通过定义一个 RC 实现 Pod 的创建过程及副本数量的自动控制。 
- RC 里包括完整的 Pod 定义模板。 
- RC 通过 Label Selector 机制实现对 Pod 副本的自动控制。 
- 通过改变 RC 里的 Pod 副本数 ，可以实现 Pod 的扩容或缩容功能。
- 通过改变 RC Pod 模板中的镜像版本，可以实现 Pod 的滚动升级功能。 



## Deployment

Deployment 是 Kubenetes v1.2 引入的新概念，引入的目的是为了更好地解决 Pod 的编排问。为此， Deployment 在内部使用了 Replica Set 来实现目的，无论从 Deployment 的作用与目的、它的 yaml 定义，还是从它的具体命令行操作来看，我们都可以把它看作是 RC 的一次升级。

Deployment 相对于 RC 的一个最大升级是我们可以随时知道当前 Pod “部署”的进度。

 Deployment 的典型使用场景有以下几个：

- 创建 Deployment 对象来生成对应的 Replica Set 并完成 Pod 副本的创建过程。
- 检查 Deployment 的状态来看部署动作是否完成（ Pod 副本的数量是否达到预期的值）。
-  更新 Deployment 以创建新的 Pod （比如镜像升级） 。
- 如果当前 Deployment 不稳定，则回滚到一个早先的 Deployment 版本。
- 暂停 Deployment 以便于一次性修改多个 PodTemplateSpec 的配置项，之后再恢复 Deployment ，进行新的发布。 
- 扩展 Deployment 以应对高负载。
-  查看 Deployment 的状态，以此作为发布是否成功的指标。 
- 清理不再需要的旧版本 ReplicaSets 

**Deployment 的简单描述文件**

```yaml
apiVersion: extensions/vlbetal        # deployment 的 api版本与之前不同
kind: Deployment
metadata:
  name: frontend
spec:
  replicas: 1
  selector: 
    matchLabels:
      tier: frontend
    matchExpressions:
      - {key: tier, operator: In, values : [frontend]}
  template:
    metadata:
      labels:
        app: app-demo
        tier: frontend
  spec:
    containers:
    - name: tomcat-demo
      image: tomcat
      imagePullPolicy: IfNotPresent      # 容器镜像的拉取策略
      ports:
      - containerPort: 8080 
```

**Deployment 常用命令**

```bash
$ kubectl create -f <dp_name>.yaml           # 根据要创建的yaml文件创建一个Deployment实例
$ kubectl get deployments                    # 查看 Deployment 的信息
$ kubectl describe deployments <dp_name>     # 查看指定的 Deployment 的详细信息
$ kubectl get rs              # 查看生成的对应的 Replic Set，它的命名与 Deploymen 的名字有关系
```

## Service(服务)

Service 也是 Kubenetes 里的最核心的资源对象之一。 Kubernetes 里的每个 Service 其实就是我们经常提起的微服务架构中的一个 “微服务” ，之前我们所说的 Pod、RC 等资源对象其实都 是为这节所说的 “服务” —Kubenetes Service 作准备的。 下图显示了 Pod、RC 与 Service 之间的逻辑关系

![3](C:\Users\李凯建\Desktop\temp\temp\3.png)

 从图中我们看到， Kubernetes Service 定义了一个服务的访问入口地址，前端的应用 ( Pod ）通过这个入口地址访问其背后的一组由 Pod 副本组成的集群实例， Service 与其后端 Pod 副本集群之间则是通过 Label Selector 来进行对接的。而 RC 的作用实际上是保证 Service 的服务能力和服务质量始终处于预期的标准。并且服务之间也是通过 TCP/IP 进行通信，从而形成了我们强大而又灵活的弹性网格。

**Service 的原理以及工作过程**

在集群中的每个 Pod 都会被分配一个单独的IP地址，而且每个 Pod 都提供了一个独立的 Endpoint ( Pod IP + ContainerPort ）以被客户端访问。但是 Pod 的 Endpoint 地址会随着 Pod 的销毁和重新创建而发生改变，因为新 Pod 的 IP 地址与之前旧 Pod 的不同。

为了访问到 Pod 上部署的服务，一 般的做法是部署一个负载均衡器（软件或硬件），为这组 Pod 开启 一个对外的服务端口如 8000 端口， 并且将这些 Pod Endpoint 列表加入 8000 端口的转发列表中，客户端就可以通过负载均衡器的对外地址＋服务端口来访问此服务，而客户端的请求最后会被转发到哪个 Pod ，则由负载均衡器的算法所决定。 

运行在每个 Node 上的 kube-proxy 进程其实就是一个智能的软件负载均衡器，它负责把对 Service 的请求转发到后端的某个 Pod 实例上，并在内部实现服务的负载均衡与会话保持机制。但 Kubenetes 发明了一种很巧妙又影响深远的设计： Service 不是共用一个负载均衡器的地址，而是每个 Service 分配了一个全局唯一的虚拟 IP 地址，这个虚拟 IP 被称为 Cluster IP。这样一来，每个服务就成了具备唯一 IP 地址的 “通信节点” ，服务调用就变成了最基础 TCP 网络通信问题。 

而且在 Service 的整个生命周期内，它的 Cluster IP 不会发生改变。于是，服务发现这个棘手的问题也得以轻松解决：只要用 Service Name 与 Service Cluster IP 地址做 DNS 域名映射即可完美解决问题。 

**Service 的简单描述文件**

```yaml
apiVersion: v1
kind: Service
metadata:
  name: tomcat-service
spec:
  ports:
  - port: 8080
    targetPort: 8080 
  selector:
    tier: frontend 
```

 在 spec.ports 的定义中， tatgetPort 属性用来确定提供该服务的容器所暴露（ EXPOSE ）的端口号，即具体业务进程在容器内的 targetPort 上提供 TCP/IP 接入：而 port 属性则定义了 Service 的虚端口。前面我们定义 tomcat 服务时，没有指定 targetPort ，则默认 targetPort 与 port 同。 

上述内容定义了一个名为“tomcat-service 的 Service ，它的服务端口为 8080 ，拥有 “ tier ontend ” 这个 Label 的所有 Pod 实例都属于它，可以运行下面的命令进行 Service 对象的创建。

```bash
$ kubectl create -f <service_file_name>.yaml
```

获取Endpoint列表：

```bash
$ kubectl  get endpoints                     # 以简单列表形式展示
$ kubectl  get svc <service_name> -o yaml    # 以 yaml 形式显示service信息(包含Cluster IP)
```

**Service 的多端口定义**

```yaml
apiVersion: v1
kind: Service
metadata:
  name: tomcat-service
spec:
  ports:
  - port: 8080
    name: service-port      # 提供服务的端口
  - port: 8005
    name: shutdown-port     # 关闭服务的端口
  selector:
    tier: frontend 
```

### 外部系统访问 Service

 Kubernetes 里包含三种IP，分别如下：

- Node IP: Node 节点的 IP 地址。 
- Pod IP: Pod 里的 IP 地址。
- Cluster IP: Service 的 IP 地址 。

首先， Node IP 是 Kubenetes 集群中每个节点的物理网卡的 IP 地址，这是一个真实存在的物理网络，所有属于这个网络的服务器之间都能通过这个网络直接通信。Kubenetes 集群之外的节点访问 Kubenetes 群之内的某个节点或者 TCP/IP 服务时，必须要通过 Node IP进行通信。 

其次， Pod IP 是每个 Pod 的 IP 地址，它是 Docker Engine 根据 docker0 网桥的 IP 地址段进行分配的，通常是一个虚拟的二层网络，Kubenetes 要求位于不同 Node 上的 Pod 能够彼此直接通信，所以 Kubenetes 里一 个Pod 里的容器访问另外一 Pod 里的容器，就是通过 Pod IP 所在的虚拟二层网络进行通信的，而真实的 TCP/IP 流量则是通过 Node 所在的物理网卡流出的 。

 最后，Service Cluster IP也是一个虚拟的 IP，但更像是一个 “伪造” 的 IP 网络，原因有以下几点：

- Cluster IP 仅仅作用 Kubenetes Service 这个对象，并由 Kubenetes 管理和分配 IP 地址 (来源于 Cluster 地址池) 。 
- Cluster IP 无法被 Ping 因为没有一个“实体网络对象”来响应。
- Cluster IP 只能结合 Service Port 组成一个具体的通信端口，单独的 Cluster IP 不具备 TCP/IP 通信的基础，并且它们属于 Kubenetes 集群这样一个封闭的空间， 集群之外的 节点如果要访问这个通信端口 ，则**需要一些额外的工作**。
- 在 Kubenetes 集群之内， Node IP 网、 Pod IP 网与 Cluster IP网之间的通信，采用的是 Kubenetes 自己设计的一种编程方式的特殊的路由规则，与我们所熟知的 IP 路由有很大的不同。 

为了能够让外部的网络直接访问到 Service 提供的服务，我们需要将 Service提供的 port 端口映射到 Node 上暴露的端口上，通过 Node IP + Node 暴露的端口号来间接访问映射的 Service 提供的服务。为实现上述功能，我们只需要对 Service 的定义文件进行扩展即可。

```yaml
apiVersion : v1
kind: Service
metadata :
  name : tomcat-service 
spec:
  type: NodePort
  ports:
    - port: 8080
      nodePort: 30001
  selector:
    tier: frontend
```

yaml 文件中定义的 `type=NodePort` 和 `nodePort=30001` 的两个属性 表明此 Service 开启了 NodPort 方式的外网访问模式，在 Kubernetes 集群之外，比如在本机的浏览器里可以通过 `Node IP + 30001` 这个地址访问 frontend（对应 8080 虚端口上）。 



## Volume( 存储卷 )

Volume 是 Pod 中能够被多个容器访问的共享目录。Kubernetes 的 Volume 概念、用途和目的与 Docker 的 Volume 比较类似，但两者不能等价。

- 首先，Kubernetes 中的 Volume 定义在 Pod 上，然后被一个 Pod 里的多个容器挂载到具体的文件目录下；
- 其次，Kubernetes 中的 Volume 与 Pod 的生命周期相同，但与容器的生命期不相关，当容器终止或者重启时，Volume 中的数据也不会丢失。
- 最后，Kubernetes 支持多种类型的 Volume， 如 GlusterFS、Ceph 等先进的分布式文件系统。

Volume 的使用也比较简单，在大多数情况下，我们先在 Pod 上声明一个 Volume，然后在容器里引用该 Volume并 Mount 到容器里的某个目录上。举例来说，我们要给之前的 Tomcat Pod 增加一个名字为 datavol 的 Volume,并且 Mount 到容器的 /mydata-data 目录上，则只要对 Pod 的定义文件做如下修正即可：

```yaml
template:
  metadata:
    labels:
      app: app-demo
      tier: frontend
spec:
  volumes:                          # 在spec中添加一个 volume 属性
    - name: datavol
      emptyDir: {}
  containers:
  - name : tomcat-demo
    image : tomcat
    volumeMounts:                   # 在容器中添加挂载的目录
      - mountPath: /mydata-data
        name: datavol
    imagePullPolicy: IfNotPresent
```

除了可以让一个 Pod 里的多个容器共享文件、让容器的数据写到宿主机的磁盘上或者写文件到网络存储中， Kubernetes 的 Volume 还扩展出了种非常有实用价值的功能，即容器配置文件集中化定义与管理，这是通过 ConfigMap 这个新的资源对象来实现的，后面我们会详细说明。 

Kubernetes 提供了非常丰富的 Volume 类型，下面对一些常用的类型逐一进行说明。

**emptyDir** 

一个emptyDir Volume 是在 Pod 分配到 Node 时创建的。从它的名称就可以看出，它的初始内容为空，并且无须指定宿主机上对应的目录文件，这是 Kubernetes 自动分配的一个目录，当 Pod 从 Node 上移除时， emptyDir 中的数据也会被永久删除。 emptyDir 的一些用途如下：

- 临时空间，例如用于某些应用程序运行时所需的临时目录，且无须永久保留。
- 长时间任务的中间过程 CheckPoint 的临时保存目录。
- 一个容器需要从另一个容器中获取数据的目录（多容器共享目录）。

目前，用户无法控制 emptyDir 使用的介质种类。如果 kubelet 的配置是使用硬盘，那么所 emptyDir 都将创建在该硬盘上。

**hostPath** 

hostPath 为在 Pod 上挂载主机上的文件或目录，它通常可以用户以下几方面：

- 容器应用程序生成的日志文件需要永久保存时，可以使用宿主机的高速晚间系统进行存储。
- 需要访问主机上 Docker 引擎内部数据结构的容器应用时，可以通过定义 hostPath 为宿主机 /var/lib/docker 目录，使容器内部应用可以直接访问 Docker 的文件系统。

在使用这种类型的 Volume 时， 需要注意 以下几点：

- 在不同的 Node 上具有相同配置的 Pod 可能会因为宿主机上的目录和文件不同而导致对 Tolume 上目录和文件的访问结果不一致。 
- 如果使用了资源配额管理，则 Kubernetes 无法将 hostPath 在宿主机上使用的资源纳入管理。 

在下面的例子中使用宿主机的／data 录定义了一 hostPath 类型的 Volume: 

```yaml
volumes:
- name : "persistent-storage”
  hostPath :
    path :”/ data ” 
```



##  Horizontal Pod Autoscaler 

 Horizontal Pod Autoscaling (Pod 横向自动扩容，简称 HPA ）与之前的 RC、Deployment 一样，也属于 Kubenetes 资源对象。通过追踪分析 RC 控制的所有目标 Pod 的负载变化情况，来确定是否需要针对性地调整目标 Pod 的副本数，这是 HPA 的实现原理。当前，HPA 可以有以下两种方式作为 Pod 负载的度量指标。 

-  CPUUtilizationPercentage。
- 应用程序自定义的度量指标，比如服务在每秒内的相应的请求数 CTPS QPS ）。  

**HPA 的描述文件**

```yaml
apiVersion : autoscaling/v1
kind: HorizontalPodAutoscaler
metadata :
  name : php-apache
  namespace: default
spec:
  maxReplicas : 10
  minReplicas : 1
  scaleTargetRef:
    kind : Deplo nent
    name : php-apache
  targetCPUUtilizationPercentage : 90 

```

*注： 如果目标 Pod 没有定义 Pod Request 值， 无法使 CPUUtilizationPercentage 实现 Pod 横向自动扩容的能力。除了使用 CPUUtilizationPercentage, Kubemete 在 v1.2版本开始尝试支持应用程序自定义的度量指标，目前仍然为实验特性，不建议在生产环境中使用。*

根据上面的定义，我们知道这个HPA 控制的对象为一个名叫 php-apache 的 Deployment 里的 Pod 副本，当这些 Pod 副本 CPUUtilizationPercentage 的值超过 90% 时会触发自动动态扩 容行为，扩容或缩容时必须满足的一个约束条件是Pod 的副本数要介于 1-10 之间。  

 除了可以通过直接定义 yaml 文件 并且调用 kubectrl create 的命令来创建一个HPA资源对象的方式，我们还能通过下面的简单命令行直接 建等价的 HPA 对象：

```bash
$ kubectl autoscale deployment php-apache --cpu-percent=90 --min=1 --max=10
```





































<font color=#999AAA >提示：这里对文章进行总结：
例如：以上就是今天要讲的内容，本文仅仅简单介绍了pandas的使用，而pandas提供了大量能使我们快速便捷地处理数据的函数和方法。</font>