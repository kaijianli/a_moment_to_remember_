# Istio

```bash
curl -L https://istio.io/downloadIstio | sh -

istioctl install --set profile=demo

get po -n istio-system

kubectl get ns

#验证Istio的安装是否正确
istioctl manifest generate --set profile=demo > $HOME/istio-generated-manifest.yaml

istioctl verify-install -f $HOME/istio-generated-manifest.yaml

# 删除bookinfo
# https://istio.io/latest/docs/examples/bookinfo/#cleanup
# samples/bookinfo/platform/kube/cleanup.sh

# 删除kiali
# kubectl delete -f samples/addons

# 开放service
# kubectl patch svc -n istio-system kiali -p '{"spec":{"type":"NodePort"}}'

# 修改访问策略
# istioctl install --set profile=demo --set meshConfig.outboundTrafficPolicy.mode=REGISTRY_ONLY
```



Istio 1.8.1 Download Complete!

Istio has been successfully downloaded into the istio-1.8.1 folder on your system.

Next Steps:
See https://istio.io/latest/docs/setup/install/ to add Istio to your Kubernetes cluster.

To configure the istioctl client tool for your workstation,
add the /root/istio-1.8.1/bin directory to your environment path variable with:
         export PATH="$PATH:/root/istio-1.8.1/bin"

Begin the Istio pre-installation check by running:
         istioctl x precheck



### 在当前bash环境下读取并执行FileName中的命令。

***\**注：该命令通常用命令“.”来替代。\****

使用范例：

source filename

. filename（中间有空格）

   source命令是bash shell的内置命令。点命令，就是个点符号，是source的另一名称。**这两个命令都以一个脚本文件名为参数，该脚本将在当前shell的环境执行，不会启动一个新的子进程，所有在脚本中设置的变量将成为当前Shell的一部分。**source（或点）命令通常用于重新执行刚修改的初始化文档。例如在登录后对 .bash_profile 中的 EDITER 变量做了修改，则用source命令重新执行 .bash_profile 中的命令而不用注销并重新登录。比如您在一个脚本里export KKK，发现没有值，假如您用source来执行 ，然后再echo，就会发现KKK=111。因为调用./a.sh来执行shell是在一个子shell里运行的，所以执行后，结构并没有反应到父shell里，但是source不同他就是在本shell中执行的，所以能够看到结果。



***\*source filename 与 sh filename 及./filename执行脚本的区别\****

1. 当shell脚本具有可执行权限时，用`sh filename`与`./filename`执行脚本是没有区别得。`./filename`是因为当前目录没有在PATH中，所有”.”是用来表示当前目录的。
2. `sh filename` 重新建立一个子shell，在子shell中执行脚本里面的语句，该子shell继承父shell的环境变量，但子shell新建的、改变的变量不会被带回父shell，除非使用export。
3. `source filename`：这个命令其实只是简单地读取脚本里面的语句依次在当前shell里面执行，没有建立新的子shell。那么脚本里面所有新建、改变变量的语句都会保存在当前shell里面。



# 修改系统时间

```bash
ntpdate ntp.api.bz
hwclock -w
```

