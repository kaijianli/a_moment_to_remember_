- transition->transition: property duration timing-function delay;

>过渡可以为一个元素在不同状态之间切换的时候定义不同的过渡效果。比如在不同的伪元素之间切换，像是 [`:hover`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/:hover)，[`:active`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/:active) 或者通过 JavaScript 实现的状态变化。
>
>transition-property	指定CSS属性的name，transition效果
>transition-duration	transition效果需要指定多少秒或毫秒才能完成
>transition-timing-function	指定transition效果的转速曲线
>transition-delay	定义transition效果开始的时候
>transition: https://developer.mozilla.org/zh-CN/docs/Web/CSS/transition
>示例：transition: margin-right 2s ease-in-out, color 1s;

- ease-in-out：规定以慢速开始和结束的过渡效果，相对于匀速，开始和结束都慢两头慢。
- overflow属性指定如果内容溢出一个元素的框，会发生什么。

#### 属性值

| 值      | 描述                                                     |
| :------ | :------------------------------------------------------- |
| visible | 默认值。内容不会被修剪，会呈现在元素框之外。             |
| hidden  | 内容会被修剪，并且其余内容是不可见的。                   |
| scroll  | 内容会被修剪，但是浏览器会显示滚动条以便查看其余的内容。 |
| auto    | 如果内容被修剪，则浏览器会显示滚动条以便查看其余的内容。 |
| inherit | 规定应该从父元素继承 overflow 属性的值。                 |

